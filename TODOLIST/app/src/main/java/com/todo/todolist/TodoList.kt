package com.todo.todolist

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.google.firebase.database.FirebaseDatabase

class TodoList: MultiDexApplication() {

   override fun onCreate() {
       super.onCreate()
       //Enable Firebase persistence for offline access
       FirebaseDatabase.getInstance().setPersistenceEnabled(true)
   }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}