package com.todo.todolist.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.todo.todolist.Interface.ItemRowListener
import com.todo.todolist.Model.ToDoItem
import com.todo.todolist.R

class ToDoItemAdapter(context: Context, toDoItemList: MutableList<ToDoItem>) : BaseAdapter() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var itemList = toDoItemList
    private var context = context
    private var rowListener: ItemRowListener = context as ItemRowListener

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val objectId: String? = itemList.get(position).objectId as String?
        val itemText: String? = itemList.get(position).itemText as String?
        val done: Boolean = itemList.get(position).done as Boolean
        val view: View
        val vh: ListRowHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_items, parent, false)
            vh = ListRowHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ListRowHolder
        }
        vh.label.text = itemText
        vh.isDone.isChecked = done

        vh.isDone.setOnClickListener {
            if (objectId != null) {
                rowListener.modifyItemState(objectId, !done)
            }
        }
        vh.edit.setOnClickListener {
            if (objectId != null) {
                if (!done) {
                    rowListener.editItemState(objectId)
                } else {
                    Toast.makeText(context, "Already Completed! If you want to update then unmark it.", Toast.LENGTH_SHORT).show()
                }
            }
        }
        vh.ibDeleteObject.setOnClickListener {
            if (objectId != null) {
                rowListener.onItemDelete(objectId)
            }
        }

        return view
    }

    override fun getItem(index: Int): Any {
        return itemList.get(index)
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getCount(): Int {
        return itemList.size
    }

    private class ListRowHolder(row: View?) {
        val label: TextView = row!!.findViewById<TextView>(R.id.tv_item_text) as TextView
        val isDone: CheckBox = row!!.findViewById<CheckBox>(R.id.cb_item_is_done) as CheckBox
        val ibDeleteObject: ImageView = row!!.findViewById<ImageView>(R.id.iv_cross) as ImageView
        val edit: ImageView = row!!.findViewById<ImageView>(R.id.iv_edit) as ImageView
    }

//    fun UpdateItem(toDoItemList: MutableList<ToDoItem>) {
//        if (toDoItemList != null) {
//            itemList.clear()
//            itemList.addAll(toDoItemList)
//            for (int )
//            notifyDataSetChanged()
//        }
//    }
}