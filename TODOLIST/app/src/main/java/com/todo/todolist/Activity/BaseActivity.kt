package com.todo.todolist.Activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.todo.todolist.Interface.BaseInterface

/**
 * Created by Wasif on 12/26/2018.
 */
abstract class BaseActivity : AppCompatActivity(), BaseInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun showProgress(flag: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}