package com.todo.todolist.Interface

interface ItemRowListener {
   fun modifyItemState(itemObjectId: String, isDone: Boolean)
   fun editItemState(itemObjectId: String)
   fun onItemDelete(itemObjectId: String)
}